user_pref("_user.js.parrot", "overrides section syntax error");

user_pref("browser.library.activity-stream.enabled", false);

user_pref("browser.privatebrowsing.autostart", true);

user_pref("extensions.update.autoUpdateDefault", false);

user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);

user_pref("browser.safebrowsing.downloads.enabled", false);

user_pref("browser.safebrowsing.downloads.remote.block_potentially_unwanted", false);
user_pref("browser.safebrowsing.downloads.remote.block_uncommon", false);

user_pref("extensions.screenshots.upload-disabled", true);

user_pref("places.history.enabled", false);

user_pref("signon.rememberSignons", false);

user_pref("browser.sessionstore.max_tabs_undo", 0);

user_pref("security.nocertdb", true);

user_pref("network.http.referer.XOriginPolicy", 2);

user_pref("network.http.referer.XOriginTrimmingPolicy", 2);

user_pref("media.autoplay.default", 5);

user_pref("dom.webnotifications.enabled", false);
user_pref("dom.webnotifications.serviceworker.enabled", false);

user_pref("dom.event.contextmenu.enabled", false);

user_pref("dom.battery.enabled", false);

user_pref("dom.vr.enabled", false);

user_pref("network.cookie.lifetimePolicy", 2);

user_pref("browser.backspace_action", 0);

user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);

user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);

user_pref("extensions.pocket.enabled", false);

user_pref("identity.fxaccounts.enabled", false);

user_pref("media.peerconnection.turn.disable", true);

user_pref("media.peerconnection.use_document_iceservers", false);

user_pref("media.peerconnection.video.enabled", false);

user_pref("media.peerconnection.identity.timeout", 1);

user_pref("privacy.trackingprotection.cryptomining.enabled", true);

user_pref("privacy.trackingprotection.fingerprinting.enabled", true);

user_pref("layout.css.devPixelsPerPx", "1.5");

user_pref("extensions.privatebrowsing.notification", true);

user_pref("extensions.enabledScopes", 15);
user_pref("extensions.autoDisableScopes", 7);

user_pref("browser.ctrlTab.recentlyUsedOrder", false);

user_pref("_user.js.parrot", "done");
