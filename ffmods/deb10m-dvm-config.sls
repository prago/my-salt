###
# Remove the mozilla directory where the profiles are stored on each start of 
# the VM to ensure fresh start
###

remove-mozilla-folder-script:
  file.append:
    - name: /rw/config/rc.local
    - text: rm -rf /home/user/.mozilla
