###
# Install the debian-10-minimal template and then clone it to make a template
# for installing the firefox mods package
###

include:
  - templates.deb10m

deb10m-ff:
  qvm.clone:
    - require:
      - sls: templates.deb10m
    - name: t-deb10m-ff
    - source: debian-10-minimal

###
# Make a disposable appVM based on debain 10 minimal template
###

{% set gui_user = salt['cmd.shell']('groupmems -l -g qubes') %}

deb10m-dvm:
  qvm.vm:
    - require:
      - deb10m-ff
    - present:
      - template: t-deb10m-ff
      - label: red
    - prefs:
      - label: red
      - dispvm-allowed: True
    - features:
      - enable:
        - appmenus-dispvm

echo -e 'firefox-esr.desktop' | qvm-appmenus --set-whitelist=- --update deb10m-dvm:
  cmd.run:
    - require:
      - deb10m-dvm
    - runas: {{ gui_user }} 
    - unless:
      - qvm-appmenus --get-whitelist deb10m-dvm | grep 'firefox-esr.desktop'
      - qvm-appmenus --get-whitelist deb10m-dvm | wc -l | grep 1
