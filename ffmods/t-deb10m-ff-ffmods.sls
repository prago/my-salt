###
# Install the firefox-esr package and make the modifications as described below
###

install-firefox-esr:
  pkg.installed:
    - pkgs:
      - firefox-esr
      - patch
      - webext-https-everywhere
      - webext-ublock-origin
      - webext-umatrix
      - pulseaudio-qubes
      - qubes-core-agent-networking
      - curl

# Set up default policies to remove unnecessary firefox features and add 
# default settings for ublock extension
firefox-esr-config:
  file.managed:
    - require:
      - install-firefox-esr
    - names:
      - /usr/lib/firefox-esr/distribution/policies.json:
        - source: salt://ffmods/policies.json
        - makedirs: True
      - /usr/lib/mozilla/managed-storage/uBlock0@raymondhill.net.json:
        - source: salt://ffmods/uBlock0@raymondhill.net.json
        - makedirs: True

# Download a user.js file to set up default settings
user-js-download:
  cmd.run:
    - require:
      - firefox-esr-config
    - name: curl --create-dirs -o /etc/firefox-esr/downloaded-userjs.js --proxy 127.0.0.1:8082 https://raw.githubusercontent.com/ghacksuserjs/ghacks-user.js/master/user.js

# Copy the contents of this user.js file over the debian defaults
user-js-config:
  cmd.run:
    - require:
      - user-js-download
    - name: sed 's|user_pref(|pref(|g' /etc/firefox-esr/downloaded-userjs.js > /etc/firefox-esr/firefox-esr.js

# Add user-overrides to the same directory
user-overrides-download:
  file.managed:
    - require:
      - user-js-config
    - names:
      - /etc/firefox-esr/user-overrides.js:
        - source: salt://ffmods/user-overrides.js

# Append the user-overrides on to the user.js file. The latest version of a 
# setting takes priority
user-overrides-js-config:
  cmd.run:
    - require:
      - user-overrides-download
    - name: sed 's|user_pref(|pref(|g' /etc/firefox-esr/user-overrides.js >> /etc/firefox-esr/firefox-esr.js

# Remove firefox default extensions
remove-xpi-firefox:
  file.absent:
    - require:
      - user-overrides-js-config
    - names:
      - /usr/lib/firefox-esr/browser/features/
      - /usr/lib/firefox-esr/browser/features/formautofill@mozilla.org.xpi
      - /usr/lib/firefox-esr/browser/features/fxmonitor@mozilla.org.xpi
      - /usr/lib/firefox-esr/browser/features/screenshots@mozilla.org.xpi
      - /usr/lib/firefox-esr/browser/features/webcompat-reporter@mozilla.org.xpi
      - /usr/lib/firefox-esr/browser/features/webcompat@mozilla.org.xpi

# Modify the umatrix defaults to stop local sites access to remote files, and 
# stop remote sites access to local files. Tighten the script settings to block
# scripts by default
modify-umatrix-defaults:
  file.patch:
    - require:
      - remove-xpi-firefox
    - name: /usr/share/webext/umatrix/js/httpsb.js
    - source: salt://ffmods/umatrix.patch

# umatrix output is garbled, see debian bug 919557 for info on this fix
fix-umatrix-919557:
  cmd.run:
    - require:
      - modify-umatrix-defaults
    - name: rm /usr/share/webext/umatrix/lib/punycode.js && cp /usr/share/javascript/punycode/punycode.js /usr/share/webext/umatrix/lib/punycode.js
