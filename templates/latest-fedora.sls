###
# install the latest fedora template described in pillar
# Execute:
#   qubesctl state.sls templates.latest-fedora dom0
###

latest-fedora-template:
  pkg.installed:
    - name: qubes-template-fedora-{{ pillar['latest-fedora'] }}
    # fromrepo: qubes-templates-itl-testing

