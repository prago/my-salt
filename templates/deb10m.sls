###
# install the debian-10-minimal template
# Execute:
#   qubesctl state.sls templates.deb10m dom0
###

template-debian-10-minimal:
  pkg.installed:
    - name: qubes-template-debian-10-minimal
    # fromrepo: qubes-templates-itl-testing

