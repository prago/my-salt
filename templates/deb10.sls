###
# install the debian-10 template
# Execute:
#   qubesctl state.sls templates.deb10 dom0
###

template-debian-10:
  pkg.installed:
    - name: qubes-template-debian-10
    # fromrepo: qubes-templates-itl-testing

