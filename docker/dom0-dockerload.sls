###
# Add a policy file to ask for the dockerload inter-vm command
###

dockerload-policy:
  file.managed:
    - name: /etc/qubes-rpc/policy/prago.Dockerload
    - contents: |
        @anyvm @anyvm ask,default_target=@dispvm:docker-dvm,user=root
