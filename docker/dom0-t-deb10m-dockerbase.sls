###
# Install the debian-10-minimal template and then clone it to make a template
# for installing the docker package
###

include:
  - templates.deb10m

deb10m-dockerbase:
  qvm.clone:
    - require:
      - sls: templates.deb10m
    - name: t-deb10m-dockerbase
    - source: debian-10-minimal

###
# Make a disposable appVM based on debain 10 minimal template
###

docker-dvm:
  qvm.vm:
    - require:
      - deb10m-dockerbase
    - present:
      - template: t-deb10m-dockerbase
      - label: red
    - prefs:
      - label: red
      - dispvm-allowed: True
    - features:
      - enable:
        - appmenus-dispvm

{% set gui_user = salt['cmd.shell']('groupmems -l -g qubes') %}

docker-dvm-addmenus:
  cmd.run:
    - require:
      - docker-dvm
    - runas: {{ gui_user }}
    - name: echo -e 'debian-uxterm.desktop' | qvm-appmenus --set-whitelist=- --update docker-dvm
    - unless:
      - qvm-appmenus --get-whitelist docker-dvm | grep 'debian-uxterm.desktop'
      - qvm-appmenus --get-whitelist docker-dvm | wc -l | grep 1

docker-increase-storage:
  cmd.run:
    - require:
      - docker-dvm-addmenus
    - name: qvm-volume extend docker-dvm:private 16001269760
    - unless:
      - qvm-volume info docker-dvm:private | grep 16001269760
