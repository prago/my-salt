###
# Install the docker package and make the modifications as described below
###


install-docker-deps:
  pkg.installed:
    - pkgs:
      - apt-transport-https
      - ca-certificates
      - curl
      - git
      - gnupg2
      - python-apt
      - qubes-core-agent-networking
      - software-properties-common
      - ssh
      - vim

# Download the debian docker gpg key
docker-gpgkey-download:
  cmd.run:
    - require:
      - install-docker-deps
    - name: curl --create-dirs -o /root/docker.gpg --proxy 127.0.0.1:8082 https://download.docker.com/linux/debian/gpg

# Check the gpg key matches the fingerprint
docker-gpgkey-check:
  cmd.run:
    - require:
      - docker-gpgkey-download
    - name: gpg --import-options show-only --import /root/docker.gpg | grep 9DC858229FC7DD38854AE2D88D81803C0EBFCD88

# Add docker gpg key
docker-gpgkey-add:
  cmd.run:
    - require:
      - docker-gpgkey-check
    - name: apt-key add /root/docker.gpg

# Add docker repo
docker-repo-add:
  pkgrepo.managed:
    - require:
      - docker-gpgkey-check
    - humanname: Docker repo
    - name: deb [arch=amd64] https://download.docker.com/linux/debian buster stable

# Install docker and other components from the docker repo
install-docker-from-repo:
  pkg.installed:
    - require:
      - docker-repo-add
    - install_recommends: False
    - pkgs:
      - docker-ce
      - docker-ce-cli
      - containerd.io
      - docker-compose

# Remove aufs-dkms as it doesn't work on qubes
remove-aufs-dkms:
  cmd.run:
    - require:
      - install-docker-from-repo
    - name: apt remove -y aufs-dkms && apt autoremove -y

# Modify the user settings to allow it to run docker (no root)
docker-usermod:
  user.present:
    - require:
      - remove-aufs-dkms
    - name: user
    - groups:
      - docker
    - remove_groups: False

# Save the docker images under /home/user/.docker
docker-uses-home-dir:
  file.managed:
    - require:
      - docker-usermod
    - names:
      - /etc/docker/daemon.json:
        - source: salt://docker/daemon.json
        - makedirs: True

