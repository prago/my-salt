###
# Add the dockerload files to the t-deb10m-dockerbase template
###

add-dockerload-files:
  file.managed:
    - names:
      - /etc/qubes-rpc/prago.Dockerload:
        - contents: |
            /usr/bin/qubes-docker-loader
      - /usr/bin/qubes-docker-load:
        - source: salt://docker/qubes-docker-load
        - mode: 755
      - /usr/bin/qubes-docker-load-wrapper:
        - source: salt://docker/qubes-docker-load-wrapper
        - mode: 755
      - /usr/bin/qubes-docker-loader:
        - source: salt://docker/qubes-docker-loader
        - mode: 755
