###
# Install the firefox package and make the modifications as described below
###


install-firefox:
  pkg.installed:
    - pkgs:
      - firefox
      - pulseaudio-qubes
      - qubes-core-agent-networking

# Set up default policies.json
firefox-config:
  file.managed:
    - require:
      - install-firefox
    - names:
      - /usr/lib64/firefox/distribution/policies.json:
        - source: salt://ffmods/policies.json
        - makedirs: True

# Download a user.js file to set up default settings
user-js-download:
  cmd.run:
    - require:
      - firefox-config
    - name: curl --create-dirs -o /etc/firefox/downloaded-userjs.js --proxy 127.0.0.1:8082 https://raw.githubusercontent.com/ghacksuserjs/ghacks-user.js/master/user.js

# Copy the contents of this user.js file over the debian defaults
user-js-config:
  cmd.run:
    - require:
      - user-js-download
    - name: sed 's|user_pref(|pref(|g' /etc/firefox/downloaded-userjs.js > /etc/firefox/pref/syspref.js

# Add user-overrides to the same directory
user-overrides-download:
  file.managed:
    - require:
      - user-js-config
    - names:
      - /etc/firefox/user-overrides.js:
        - source: salt://ffmods/user-overrides.js

# Append the user-overrides on to the user.js file. The latest version of a 
# setting takes priority
user-overrides-js-config:
  cmd.run:
    - require:
      - user-overrides-download
    - name: sed 's|user_pref(|pref(|g' /etc/firefox/user-overrides.js >> /etc/firefox/pref/syspref.js

