###
# Make a disposable appVM based on latest fedora template from pillar
###

{% set gui_user = salt['cmd.shell']('groupmems -l -g qubes') %}

include:
  - templates.latest-fedora

t-fedora-dvm:
  qvm.clone:
    - require:
      - sls: templates.latest-fedora
    - name: t-fed{{ pillar['latest-fedora'] }}-dvm
    - source: fedora-{{ pillar['latest-fedora'] }}

fedora-{{ pillar['latest-fedora'] }}-dvm:
  qvm.vm:
    - require:
      - t-fedora-dvm
    - present:
      - template: t-fed{{ pillar['latest-fedora'] }}-dvm
      - label: red
    - prefs:
      - label: red
      - dispvm-allowed: True
    - features:
      - enable:
        - appmenus-dispvm

echo -e 'firefox.desktop' | qvm-appmenus --set-whitelist=- --update fedora-{{ pillar['latest-fedora'] }}-dvm:
  cmd.run:
    - require:
      - fedora-{{ pillar['latest-fedora'] }}-dvm
    - runas: {{ gui_user }} 
    - unless:
      - qvm-appmenus --get-whitelist fedora-{{ pillar['latest-fedora'] }}-dvm | grep 'firefox.desktop'
      - qvm-appmenus --get-whitelist fedora-{{ pillar['latest-fedora'] }}-dvm | wc -l | grep 1

set-default-mgmt-template:
  qvm.prefs:
    - require:
      - fedora-{{ pillar['latest-fedora'] }}-dvm
    - name: default-mgmt-dvm
    - template: fedora-{{ pillar['latest-fedora'] }}
    - unless:
      - qvm-prefs --get default-mgmt-dvm template | grep deb

set-default-template:
  cmd.run:
    - require:
      - fedora-{{ pillar['latest-fedora'] }}-dvm
    - name: qubes-prefs --set default_template fedora-{{ pillar['latest-fedora'] }}
    - unless:
      - qubes-prefs --get default_template | grep "deb\|offline\|fedora-{{ pillar['latest-fedora'] }}"

set-default-dvm-template:
  cmd.run:
    - require:
      - fedora-{{ pillar['latest-fedora'] }}-dvm
    - name: qubes-prefs --set default_dispvm fedora-{{ pillar['latest-fedora'] }}-dvm
    - unless:
      - qubes-prefs --get default_dispvm | grep "deb\|offline\|fedora-{{ pillar['latest-fedora'] }}-dvm"

