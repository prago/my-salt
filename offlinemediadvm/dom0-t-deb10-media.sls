###
# Make a disposable appVM based on debian 10 for offline media
###

{% set gui_user = salt['cmd.shell']('groupmems -l -g qubes') %}

include:
  - templates.deb10

t-deb10-media:
  qvm.clone:
    - require:
      - sls: templates.deb10
    - source: debian-10

offline-media-dvm:
  qvm.vm:
    - require:
      - t-deb10-media
    - present:
      - template: t-deb10-media
      - label: red
    - prefs:
      - label: red
      - dispvm-allowed: True
      - netvm: ''
    - features:
      - enable:
        - appmenus-dispvm

set-default-dvm:
  cmd.run:
    - require:
      - offline-media-dvm
    - name: qubes-prefs --set default_dispvm offline-media-dvm
    - unless:
      - qubes-prefs --get default_dispvm | grep offline-media-dvm

echo -e 'debian-uxterm.desktop\nveracrypt.desktop' | qvm-appmenus --set-whitelist=- --update offline-media-dvm:
  cmd.run:
    - require:
      - offline-media-dvm
    - runas: {{ gui_user }} 
    - unless:
      - qvm-appmenus --get-whitelist offline-media-dvm | grep 'debian-uxterm.desktop'
      - qvm-appmenus --get-whitelist offline-media-dvm | grep 'veracrypt.desktop'
      - qvm-appmenus --get-whitelist offline-media-dvm | wc -l | grep 2

