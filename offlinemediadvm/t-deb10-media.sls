###
# Install the veracrypt package and media player
###


install-media-deps:
  pkg.installed:
    - pkgs:
      - curl
      - git
      - gnupg2
      - mpv
      - qubes-core-agent-networking
      - vifm
      - vim

# Download the veracrypt gpg key
vc-gpgkey-download:
  cmd.run:
    - require:
      - install-media-deps
    - name: curl --create-dirs -o /root/veracrypt.gpg --proxy 127.0.0.1:8082 https://idrix.fr/VeraCrypt/VeraCrypt_PGP_public_key.asc

# Check the gpg key matches the fingerprint
vc-gpgkey-check:
  cmd.run:
    - require:
      - vc-gpgkey-download
    - name: gpg --import-options show-only --import /root/veracrypt.gpg | grep 5069A233D55A0EEB174A5FC3821ACD02680D16DE

# Add veracrypt gpg key
vc-gpgkey-add:
  cmd.run:
    - require:
      - vc-gpgkey-check
    - name: gpg --import /root/veracrypt.gpg

download-veracrypt:
  cmd.run:
    - require:
      - vc-gpgkey-add
    - name: curl --create-dirs -L -o /tmp/veracrypt-1.24-u4-deb10.deb --proxy 127.0.0.1:8082 https://launchpad.net/veracrypt/trunk/1.24-update4/+download/veracrypt-1.24-Update4-Debian-10-amd64.deb && sha256sum /tmp/veracrypt-1.24-u4-deb10.deb | grep ebf41918434f8241c2926adc1d0aba7d629c241b86120eb31acdab0cd9dafeb7
    - unless:
      - dpkg -l | grep veracrypt

download-veracrypt-sig:
  cmd.run:
    - require:
      - vc-gpgkey-add
    - name: curl --create-dirs -L -o /tmp/veracrypt-1.24-u4-deb10.deb.sig --proxy 127.0.0.1:8082 https://launchpad.net/veracrypt/trunk/1.24-update4/+download/veracrypt-1.24-Update4-Debian-10-amd64.deb.sig && sha256sum /tmp/veracrypt-1.24-u4-deb10.deb.sig | grep 3901f3bac900e52bd24b199a29b85c768d29b37c92cf15e19726cc054b5b8765
    - unless:
      - dpkg -l | grep veracrypt

verify-veracrypt-dl:
  cmd.run:
    - require:
      - download-veracrypt
      - download-veracrypt-sig
    - name: gpg --verify /tmp/veracrypt-1.24-u4-deb10.deb.sig /tmp/veracrypt-1.24-u4-deb10.deb
    - unless:
      - dpkg -l | grep veracrypt

install-veracrypt:
  pkg.installed:
    - require:
      - verify-veracrypt-dl
    - sources:
      - veracrypt: /tmp/veracrypt-1.24-u4-deb10.deb
    - unless:
      - dpkg -l | grep veracrypt
