add-different-repos:
  cmd.run:
    - name: |
        sed 's|buster main|testing main|g' /etc/apt/sources.list | grep testing >> /etc/apt/sources.list 
        sed 's|buster main|unstable main|g' /etc/apt/sources.list | grep unstable >> /etc/apt/sources.list 
        sed 's|buster main|experimental main|g' /etc/apt/sources.list | grep experimental >> /etc/apt/sources.list 
    - unless: 'test -e /etc/apt/preferences'

set-repo-pins:
  file.managed:
    - require:
      - add-different-repos
    - names:
      - /etc/apt/preferences:
        - source: salt://apt-prefs/apt-prefs
    - creates: /etc/apt/preferences
