###
# Add dependencies for trezor gpg
###

install-trezor-gpg-deps:
  pkg.installed:
    - pkgs:
      - gnupg2
      - libudev-dev
      - libusb-1.0-0-dev
      - python3-dev
      - python3-pip
      - python3-tk
      - qubes-core-agent-networking

trezor-agent-pip-deps:
  pip.installed:
    - require:
      - install-trezor-gpg-deps
    - proxy: 127.0.0.1:8082
    - names:
      - Cython
      - hidapi
      - pymsgbox==1.0.6

install-trezor-agent:
  pip.installed:
    - require:
      - trezor-agent-pip-deps
    - proxy: 127.0.0.1:8082
    - name: trezor_agent

copy-usrlocal-to-usrlocalorig:
  cmd.run:
    - require:
      - install-trezor-agent
    - name: cp -rf /usr/local/* /usr/local.orig/
    - unless:
      - ls /usr/local.orig/bin/trezor-gpg

create-unlock-desktop-file:
  file.managed:
    - require:
      - copy-usrlocal-to-usrlocalorig
    - makedirs: True
    - name: /usr/share/applications/unlock-trezor.desktop
    - source: salt://trezor/unlock-trezor.desktop

###
# Add dependencies for split gpg
###

install-split-gpg-deps:
  pkg.installed:
    - pkgs:
      - qubes-gpg-split

