###
# Ensure trezor-unlock is avaiable in shortcuts
###

{% set gui_user = salt['cmd.shell']('groupmems -l -g qubes') %}

trzr-sync-appmenus:
  cmd.run:
    - name: |
        qvm-start --skip-if-running t-deb10m-trzr-gpg
        qvm-sync-appmenus t-deb10m-trzr-gpg
        qvm-shutdown --wait t-deb10m-trzr-gpg
    - runas: {{ gui_user }}
    - unless:
      - ls /home/{{ gui_user }}/.local/share/qubes-appmenus/t-deb10m-trzr-gpg/apps.templates/unlock-trezor.desktop

###
# Make an appVM based on t-deb10m-trzr-gpg
###

{% for vault in pillar.get('vaults') %}

{{ vault }}:
  qvm.vm:
    - present:
      - template: t-deb10m-trzr-gpg
      - label: black
    - prefs:
      - netvm: ''
      - maxmem: 400

echo -e 'unlock-trezor.desktop\ndebian-uxterm.desktop' | qvm-appmenus --set-whitelist=- --update {{ vault }}:
  cmd.run:
    - requires:
      - {{ vault }}
      - trzr-sync-appmenus
    - runas: {{ gui_user }}
    - unless:
      - ls /home/{{ gui_user }}/.local/share/qubes-appmenus/{{ vault }}/apps/{{ vault }}-unlock-trezor.desktop

{% endfor %}
