###
# Allow communication with trezord running in sys-usb via qrexec
###

/rw/config/rc.local:
  file.append:
    - text:
      - socat TCP-LISTEN:21325,fork EXEC:"qrexec-client-vm sys-usb trezord-service" &
