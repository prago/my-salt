###
# Add the trezord policy file to dom0
###

/etc/qubes-rpc/policy/trezord-service:
  file.managed:
    - contents:
      - $anyvm $anyvm ask,user=trezord,default_target=sys-usb

{% set default_vault = pillar.get('vaults')[0] %}

/etc/qubes-rpc/policy/qubes.Gpg:
  file.managed:
    - contents:
      - $anyvm $anyvm ask,default_target={{ default_vault }}
