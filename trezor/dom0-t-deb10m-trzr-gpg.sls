###
# Clone the debian-10-minimal template 
# Then make an appVM based on it
###

include:
  - templates.deb10m

t-deb10m-trzr-gpg:
  qvm.clone:
    - require:
      - sls: templates.deb10m
    - name: t-deb10m-trzr-gpg
    - source: debian-10-minimal
