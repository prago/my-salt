###
# Install the debian-10 template and clone it to make a template for hardware
# wallets to be accessed from sys-usb
###

include:
  - templates.deb10

dom0-deb10-hww-usb:
  qvm.clone:
    - require:
      - sls: templates.deb10
    - name: t-deb10-hww-usb
    - source: debian-10

sys-usb-set-template:
  qvm.prefs:
    - require:
      - dom0-deb10-hww-usb
    - name: sys-usb
    - template: t-deb10-hww-usb

