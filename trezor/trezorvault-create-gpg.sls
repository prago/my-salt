###
# Unlock the trezor before running the salt
# Creates a gpg key given the information in the pillar
# The following command fails if 'Unlock Trezor' has not already ran
###

trezorgpg-init:
  cmd.run:
    - name: trezor-gpg init {{ pillar['trezor-gpg-init-cmd'] }} || ( rm -rf /home/user/.gnupg/trezor && exit 1)
    - runas: user
    - unless:
      - ls /home/user/.gnupg/trezor

/home/user/.bashrc:
  file.append:
    - require:
      - trezorgpg-init
    - text:
      - export GNUPGHOME=~/.gnupg/trezor
