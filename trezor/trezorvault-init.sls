###
# Set gnupghome to allow split-gpg to use the trezor 
###

/home/user/.profile:
  file.append:
    - text:
      - export GNUPGHOME=~/.gnupg/trezor

