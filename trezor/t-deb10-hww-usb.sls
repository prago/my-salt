###
# Install the required packages into the t-deb10-hww-usb template for use with
# trezor
###

sys-usb-pkg-curl:
  pkg.installed:
    - names:
      - curl

download-trezor-bridge:
  cmd.run:
    - require:
      - sys-usb-pkg-curl
    - name: curl --create-dirs -o /tmp/trezor-bridge-amd64.deb --proxy 127.0.0.1:8082 https://wallet.trezor.io/data/bridge/2.0.27/trezor-bridge_2.0.27_amd64.deb && sha256sum /tmp/trezor-bridge-amd64.deb | grep ed7edb5cb33210af6faaf8a10f219525d8a1e551ea6f886cc6654b427f91e397
    - unless:
      - dpkg -l | grep trezor

install-trezor-bridge:
  pkg.installed:
    - require:
      - download-trezor-bridge
    - sources:
      - trezor-bridge: /tmp/trezor-bridge-amd64.deb
    - unless:
      - dpkg -l | grep trezor

download-udev-rules:
  cmd.run:
    - require:
      - install-trezor-bridge
    - name: curl --create-dirs -o /etc/udev/rules.d/51-trezor.rules --proxy 127.0.0.1:8082 https://raw.githubusercontent.com/trezor/trezor-common/master/udev/51-trezor.rules && sha256sum /etc/udev/rules.d/51-trezor.rules | grep f3b39b4537da6260a7b63af07710aedfab1fe87d8eeff0975d176b9b908b9d6e
    - creates: /etc/udev/rules.d/51-trezor.rules
