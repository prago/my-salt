###
# Install the required packages into the deb10m-heads template
###

install-heads-deps:
  pkg.installed:
    - pkgs:
      - bc
      - bison
      - build-essential
      - bzip2
      - ccache
      - cmake
      - cpio
      - flex
      - git
      - gnat
      - gnupg
      - libdigest-sha-perl
      - libelf-dev
      - libusb-1.0-0-dev
      - m4
      - nasm
      - patch
      - pkg-config
      - python
      - qubes-core-agent-networking
      - texinfo
      - uuid-dev
      - vim
      - wget
      - zlib1g-dev
