###
# Get the heads-builder appVM ready to build heads
###

git-pull-heads:
  git.latest:
    - name: https://github.com/osresearch/heads.git
    - target: /home/user/heads
    - user: user
