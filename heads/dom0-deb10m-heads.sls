###
# Install the debian-10-minimal template and then clone it to make a template
# for building heads
###

include:
  - templates.deb10m

dom0-deb10m-heads:
  qvm.clone:
    - require:
      - sls: templates.deb10m
    - name: t-deb10m-heads
    - source: debian-10-minimal

###
# Make an appVM based on t-deb10m-heads to build heads in
###

heads-builder-present:
  qvm.present:
    - require:
      - dom0-deb10m-heads
    - name: heads-builder
    - template: t-deb10m-heads
    - label: gray

heads-builder-increase-storage:
  cmd.run:
    - require:
      - heads-builder-present
    - name: qvm-volume extend heads-builder:private 16001269760
    - unless:
      - qvm-volume info heads-builder:private | grep 16001269760
