###
# Install the debian-10 template and then clone it to make a template
# for installing the wireguard package
###

include:
  - templates.deb10

deb10-wg:
  qvm.clone:
    - require:
      - sls: templates.deb10
    - name: t-deb10-wg
    - source: debian-10

deb10-wg-prefs:
  qvm.prefs:
    - require:
      - deb10-wg
    - name: t-deb10-wg
    - kernel: ''
    - virt-mode: hvm

###
# Make a sys-vpn based on t-deb10-wg
###

{% for wgvm in pillar.get('wg-vms') %}

sys-vpn-{{ wgvm }}:
  qvm.vm:
    - require:
      - deb10-wg-prefs
    - present:
      - template: t-deb10-wg
      - label: green
      - flags:
        - proxy
    - prefs:
      - label: green
      - netvm: sys-net
      - kernel: ''
      - virt-mode: hvm
      - flags:
        - proxy
    - service:
      - enable:
        - vpn-handler-openvpn-wg

{% endfor %}

