###
# Install the wireguard package and install the qubes-vpn-support package
###

install-deps:
  pkg.installed:
    - pkgs:
      - curl
      - qubes-core-agent-networking
      - tar

resolvconf:
  pkg.installed

install-wg:
  pkg.installed:
    - require:
      - install-deps
      - resolvconf
    - refresh: True
    - pkgs:
      - wireguard

# Download the v1.4.2 release and extract it
qubes-vpn-support-download:
  cmd.run:
    - require:
      - install-wg
    - name: curl -L -o /opt/qubes-vpn.tar.gz --proxy 127.0.0.1:8082 https://github.com/tasket/Qubes-vpn-support/archive/v1.4.2.tar.gz && tar -xzvf qubes-vpn.tar.gz
    - cwd: /opt/
    - creates: /opt/qubes-vpn.tar.gz

# Run the install script
install-qubes-vpn:
  cmd.run:
    - require:
      - qubes-vpn-support-download
    - name: bash ./install
    - cwd: /opt/Qubes-vpn-support-1.4.2
    - creates: /usr/lib/qubes/qubes-vpn-setup

set-vpn-to-wg:
  cmd.run:
    - require:
      - install-qubes-vpn
    - name: cp /usr/lib/systemd/system/qubes-vpn-handler.service.d/10_wg.conf.example /usr/lib/systemd/system/qubes-vpn-handler.service.d/10_wg.conf
