###
# Add the config files to sys-vpn 
###

link-firewall-vpn:
  file.symlink:
    - name: /rw/config/qubes-firewall.d/90_tunnel-restrict
    - target: /usr/lib/qubes/proxy-firewall-restrict 
    - force: True
    - makedirs: True 

# Copy the config file and password file to sys-vpn
vpn-config:
  file.managed:
    - require:
      - link-firewall-vpn
    - names:
      - /rw/config/vpn/no-userpassword.txt:
        - contents: 
          - ''
        - makedirs: True
        - mode: 600
      - /rw/config/vpn/vpn-client.conf:
        - contents_pillar: wg-conf
        - makedirs: True
