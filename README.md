# My qubes salt config

To use these files, copy the contained files to /srv/salt/ 
(so ffmods.top should be located at /srv/salt/ffmods.top)
and then enable the states before applying the highstate:


### ffmods


This sets up a disposable vm based on the debian-10-minimal template using 
firefox-esr


From dom0 run the following once the files are located where 
they should be:


`[dom0 $] sudo qubesctl top.enable ffmods`


`[dom0 $] sudo qubesctl --show-output --targets dom0,t-deb10m-ff,deb10m-dvm state.highstate`


### dockerdvm


This sets up a disposable vm based on the debian-10-minimal template with 
docker installed. This can be used to download docker images to transfer to 
offline vms. The second command needs to be ran twice due to an error


`[dom0 $] sudo qubesctl top.enable dockerdvm`


`[dom0 $] sudo qubesctl --show-output --targets dom0,t-deb10m-dockerbase state.highstate`


### qubesvpn


This sets up vms based on the debian-10 template with wireguard installed. To
do this, it needs to be a HVM and use the kernel in the VM. It then installs
the qubes-vpn-support package (https://github.com/tasket/qubes-vpn-support)
and sets up the config files. Place your config files in a salt pillar under
the wg-conf variable and specify which VMs to create in the wg-vms variable

For example:
```
/srv/pillar/wg.top
base:
  dom0:
    - wg
  sys-vpn-local:
    - wg-local
  sys-vpn-uk:
    - wg-uk
```

```
/srv/pillar/wg.sls
wg-vms:
  - local
  - uk
```


```
/srv/pillar/wg-local.sls
wg-conf: |
    [Interface]
    PrivateKey=
    ...
    [Peer]
    PublicKey=
```




`[dom0 $] sudo ln -s /srv/pillar/wg.top /srv/pillar/_tops/base/wg.top`

Then run the following 

`[dom0 $] sudo qubesctl top.enable qubesvpn`


`[dom0 $] sudo qubesctl --show-output --targets dom0,t-deb10-wg,sys-vpn-local state.highstate`


### dockerload


This sets up the docker-dvm and VMs based on t-deb10m-dockerbase to pass docker
images between themselves and load them in the dispVM


`[dom0 $] sudo qubesctl top.enable dockerload`


`[dom0 $] sudo qubesctl --show-output --targets dom0,t-deb10m-dockerbase state.highstate`


### Heads


This sets up a heads-builder appVM based on t-deb10m-heads to build heads
(https://github.com/osresearch/heads)


`[dom0 $] sudo qubesctl top.enable headsbuilder`


`[dom0 $] sudo qubesctl --show-output --targets dom0,t-deb10m-heads,heads-builder state.highstate`


Once the VMs are in the correct state, pull the latest commit and build:


`[head $] cd heads && git pull`


`[head $] make BOARD=x230`


### Offline media DVM


Set up a debian based DVM with mpv and veracrypt and use this as the default
disposable VM template


`[dom0 $] sudo qubesctl top.enable offlinemediadvm`


`[dom0 $] sudo qubesctl --show-output --targets dom0,t-deb10-media state.highstate`



### Add default DVM


Install the latest fedora template, defined in a pillar, set this as the
default template and create a dvm based on it.

Example pillar:
```
/srv/pillar/latest-os.top
base:
  dom0:
    - latest-os
```

```
/srv/pillar/latest-os.top
latest-fedora: 32
```

Link the file before running the following

`[dom0 $] sudo ln -s /srv/pillar/latest-os.top /srv/pillar/_tops/base/`


`[dom0 $] sudo qubesctl top.enable add-default-dvm`


`[dom0 $] sudo qubesctl --show-output --targets dom0,t-fed32-dvm state.highstate`



### Create a trezorvault VM to be used with qubes-split-gpg


Define the trezorvault options in the following pillars:
```
/srv/pillar/trezorvault.top
base:
  dom0:
    - trezorvault
```

```
/srv/pillar/trezorvault.sls
vaults:
  - trezorvault-1
  - trezorvault-2
  ...
```

```
/srv/pillar/trezorgpg.top
base:
  trezorvault-1:
    - trezorgpg-1
  trezorvault-2:
    - ...
```

```
/srv/pillar/trezorgpg-1.sls
trezor-gpg-init-cmd: '"asdf <asdf@example.com>" --time=0'
```

`[dom0 $] sudo ln -s /srv/pillar/trezorvault.top /srv/pillar/_tops/base/`

`[dom0 $] sudo ln -s /srv/pillar/trezorgpg-1.top /srv/pillar/_tops/base/`


The above pillar will create two trezorvault VMs, trezorvault-1 and 
trezorvault-2, and the trezor-gpg-init-cmd will be applied to the first VM

Note: The first vault in the pillar will be the default

This salt has three stages, creating the base templates, setting up the vaults
to access the trezor via sys-usb, and finally creating the GPG key. They must
be completed in the correct order, and when re-creating, it may be neccessary
to disable the later top files


`[dom0 $] sudo qubesctl top.enable trezorbase`


`[dom0 $] sudo qubesctl --show-output --targets dom0,t-deb10-hww-usb,t-deb10m-trzr-gpg,sys-usb state.highstate`


`[dom0 $] sudo qubesctl top.enable trezorvault`


`[dom0 $] sudo qubesctl --show-output --targets dom0,trezorvault-1,trezorvault-2,... state.highstate`


It is then required to unlock the trezor using the menu shortcut, leaving the 
domain running before executing the next step. During the next step the trezor
will prompt to confirm on the device twice


`[dom0 $] sudo qubesctl top.enable trezorbase`


`[dom0 $] sudo qubesctl --show-output --targets dom0,t-deb10-hww-usb,t-deb10m-trzr-gpg,sys-usb state.highstate`


To allow another VM to sign using the trezor and split-gpg, add the name of the
vault to /rw/config/gpg-split-domain within the domain. Then run the unlock
shortcut and enter the pin/password if required. Test with the following:

`[test $] echo "123" | qubes-gpg-client-wrapper --clearsign | qubes-gpg-client-wrapper --verify`


To allow signed git commits, add the following to the git VMs /home/user/.gitconfig:

```
[user]
name = asdf
email = asdf@example.com
signingkey = <last eight digits of public key>

[gpg]
program = qubes-gpg-client-wrapper

[commit]
gpgsign = true
```


Signing is then done when running 

`[git $] git commit -a -m 'Test commit'`

